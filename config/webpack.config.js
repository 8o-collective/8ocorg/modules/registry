// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin");

const createBuildTemplate = require("template");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

module.exports = createBuildTemplate(resolveAppPath, {
  resolve: {
    fallback: {
      fs: false,
      WNdb: false,
      lapack: false,
    },
  },
  module: {
    rules: [
      {
        test: /\.md$/,
        use: "raw-loader",
      },
      {
        test: /\.(png|webp)$/,
        use: "file-loader",
      },
    ],
  },
  plugins: [new NodePolyfillPlugin()],
});
