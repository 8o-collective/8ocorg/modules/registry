import React, { useState, useEffect } from "react";

import {
  PercolatorContainer,
  PercolatorWord,
} from "assets/styles/Percolator.styles.jsx";

const Percolator = ({
  words,
  numActiveWords,
  baselineSpeed,
  speedExponent,
  direction,
}) => {
  const [activeWords, setActiveWords] = useState([]);

  const generateWord = () => {
    const word = words[Math.floor(Math.random() * words.length)];
    const x = Math.random() * 100; // random position between 0% and 100%
    const y = Math.pow(Math.random(), 3) * 100; // random position between 0% and 100%
    const id = Math.random();
    const timeout = baselineSpeed / y ** speedExponent - 0.9;

    return { id, word, x, y, timeout };
  };

  useEffect(() => {
    for (let i = 0; i < numActiveWords; i++) {
      const newWord = generateWord();

      setTimeout(() => {
        setActiveWords((prevActiveWords) => {
          const filteredWords = prevActiveWords.filter(
            (word) => word.id !== newWord.id
          );
          return [...filteredWords, generateWord()];
        });
      }, newWord.timeout * 1000 + 500);

      setActiveWords((prevActiveWords) => [...prevActiveWords, newWord]);
    }
  }, []);

  return (
    <PercolatorContainer>
      {activeWords.map(({ id, word, x, y, timeout }) => (
        <PercolatorWord
          key={id}
          direction={direction}
          style={{
            animationDuration: `${timeout}s`,
            [direction === "left" ? "right" : "left"]: `${
              direction === "left" ? y : x
            }%`,
            bottom: `${(direction === "left" ? x : y) - id}%`,
          }}
        >
          {word}
        </PercolatorWord>
      ))}
    </PercolatorContainer>
  );
};

export default Percolator;
