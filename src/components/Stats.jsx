import React, { useState, useEffect } from "react";

import {
  StatsContainer,
  StatsBar,
  StatsBarlabelContainer,
  StatsAttributeContainer,
  StatsCoordinatesContainer,
  StatsCoordinatesHyperlink,
  StatsTileHeader,
  StatsRelatedUserTable,
  StatsRelatedUserTableHeader,
  StatsRelatedUserTableBody,
  StatsRelatedUserLink,
  StatsTableRow,
  StatsTableCell,
  StatsTableHeader,
  StatsTableDiv,
  StatsMainDiv,
  StatsMainContainer,
  StatsMemberForDiv,
} from "assets/styles/Stats.styles.jsx";

import { getStats } from "actions/stats.js";

const mapZoomLevel = "7.0z";

const Stats = ({ id }) => {
  const [stats, setStats] = useState(undefined);
  const [blink, setBlink] = useState(true);

  useEffect(() => {
    const userStats = getStats(id);

    setStats(userStats);

    if (userStats === undefined) return;

    const interval =
      getStats(id) !== null
        ? setInterval(
            () =>
              setStats((prevStats) => ({
                ...prevStats,
                memberFor: prevStats["memberFor"] + 1,
              })),
            1000
          )
        : setInterval(() => setBlink((prevBlink) => !prevBlink), 500);

    return () => clearInterval(interval);
  }, []);

  if (stats === undefined) return <></>;
  if (stats === null)
    return (
      <StatsContainer style={{ color: "red", fontSize: "12vw" }}>
        {blink && "DATA ENCRYPTED"}
      </StatsContainer>
    );

  return (
    <StatsContainer>
      <StatsTableDiv>
        <StatsTileHeader>Influences</StatsTileHeader>
        <StatsRelatedUserTable color={"red"}>
          <StatsRelatedUserTableHeader>
            <StatsTableRow color={"red"}>
              <StatsTableHeader>Name</StatsTableHeader>
              <StatsTableHeader>Valence</StatsTableHeader>
              <StatsTableHeader>Intensity</StatsTableHeader>
            </StatsTableRow>
          </StatsRelatedUserTableHeader>
        </StatsRelatedUserTable>
        <StatsRelatedUserTable color={"#1c1c1c"} padding={"5px"}>
          <StatsRelatedUserTableBody>
            {[0, 1, 2, 3, 4, 5].map((i) => (
              <StatsTableRow key={i} color={"#1c1c1c"}>
                <StatsTableCell>
                  <StatsRelatedUserLink
                    href={"./" + stats.relatedUsers[i].name}
                  >
                    {stats.relatedUsers[i].name}
                  </StatsRelatedUserLink>
                </StatsTableCell>
                <StatsTableCell>{stats.relatedUsers[i].valence}</StatsTableCell>
                <StatsTableCell>
                  {stats.relatedUsers[i].intensity}
                </StatsTableCell>
              </StatsTableRow>
            ))}
          </StatsRelatedUserTableBody>
        </StatsRelatedUserTable>
      </StatsTableDiv>

      <StatsMainDiv>
        <StatsTileHeader>Stats</StatsTileHeader>
        <StatsMainContainer>
          {[0, 1, 2].map((i) => (
            <StatsAttributeContainer key={i}>
              <StatsBarlabelContainer>
                {Object.keys(stats.qualities)[i]}:
              </StatsBarlabelContainer>

              <StatsBar
                color="red"
                progress={Object.values(stats.qualities)[i]}
              />
            </StatsAttributeContainer>
          ))}
          <StatsCoordinatesContainer>
            Estimated coordinates:{" "}
            <StatsCoordinatesHyperlink
              href={`https://www.google.com/maps/place/${
                stats.coordinates.dms
              }/@${[...stats.coordinates.decimal, mapZoomLevel]}`}
            >
              {stats.coordinates.dms}
            </StatsCoordinatesHyperlink>
          </StatsCoordinatesContainer>
        </StatsMainContainer>
      </StatsMainDiv>

      <StatsMemberForDiv>
        <StatsAttributeContainer>
          Member for: {stats.memberFor} seconds
        </StatsAttributeContainer>
      </StatsMemberForDiv>
    </StatsContainer>
  );
};

export default Stats;
