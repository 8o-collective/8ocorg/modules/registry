import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import { getShard } from "@8ocorg/communicator";

import {
  ProfileContainer,
  ProfilePercolationContainer,
  ProfileTitleContainer,
  ProfileDescriptionContainer,
  ProfileStatsContainer,
  ProfileBackLink,
} from "assets/styles/Profile.styles.jsx";

import Percolator from "components/Percolator.jsx";
import Stats from "components/Stats.jsx";

import { getDescription } from "actions/descriptions.js";

const Profile = () => {
  let { id } = useParams();
  const [description, setDescription] = useState("");

  useEffect(() => {
    const desc = getDescription(id);

    if (desc === null) {
      getShard().then((shard) => {
        setDescription(`${shard} `.repeat(9999));
      });
    } else {
      setDescription(desc);
    }
  }, []);

  return (
    <ProfileContainer>
      <ProfileTitleContainer>{id}</ProfileTitleContainer>
      <ProfilePercolationContainer>
        <Percolator
          words={["you", "can", "always", "be", "found"]}
          numActiveWords={100}
          baselineSpeed={1000000}
          speedExponent={2}
          direction="left"
        />
      </ProfilePercolationContainer>
      <ProfileDescriptionContainer>{description}</ProfileDescriptionContainer>
      <ProfileStatsContainer>
        <Stats id={id} />
      </ProfileStatsContainer>
      <ProfileBackLink href="/"> {"< Back"} </ProfileBackLink>
    </ProfileContainer>
  );
};

export default Profile;
