import React, { useState, useEffect } from "react";

import {
  AppContainer,
  AppWelcomeContainer,
  AppWelcomeTitle,
  AppWelcomeDescription,
  AppWelcomeStrong,
} from "assets/styles/App.styles.jsx";

import { generateKeyname } from "actions/keyname.js";

import Globe from "components/Globe.jsx";
import Search from "components/Search.jsx";
import UserList from "components/UserList.jsx";

const App = () => {
  const [dimensions, setDimensions] = useState({
    height: window.innerHeight,
    width: window.innerWidth,
  });

  useEffect(() => {
    if (!localStorage.getItem("keyname")) generateKeyname();
  }, []);

  useEffect(() => {
    const handleResize = () =>
      setDimensions({
        height: window.innerHeight,
        width: window.innerWidth,
      });

    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return (
    <AppContainer>
      {dimensions.width > dimensions.height && <Globe />}
      <AppWelcomeContainer>
        <AppWelcomeTitle>Welcome to the Registry</AppWelcomeTitle>
        <AppWelcomeDescription>
          a database of all members, in{" "}
          <AppWelcomeStrong>all possible realities.</AppWelcomeStrong>
        </AppWelcomeDescription>
      </AppWelcomeContainer>
      <Search />
      <UserList />
    </AppContainer>
  );
};

export default App;
