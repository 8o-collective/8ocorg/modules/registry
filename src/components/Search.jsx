import React, { useState } from "react";

import {
  SearchContainer,
  SearchInput,
  SearchInputLabel,
  SearchInputSubmit,
  SearchForm,
} from "assets/styles/Search.styles.jsx";

const Search = () => {
  const [user, setUser] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    window.location.href += user;
  };

  return (
    <SearchContainer>
      <SearchForm onSubmit={handleSubmit}>
        <SearchInputLabel>
          <SearchInput
            type="search"
            placeholder="Type username here..."
            value={user}
            onChange={(e) => setUser(e.target.value)}
          />
        </SearchInputLabel>
        <SearchInputSubmit type="submit" value="Discover" />
      </SearchForm>
    </SearchContainer>
  );
};

export default Search;
