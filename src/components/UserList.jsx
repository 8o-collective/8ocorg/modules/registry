import React, { useState, useEffect, useRef } from "react";

import {
  UserListContainer,
  UserListLink,
  UserListList,
  UserListTitle,
} from "assets/styles/UserList.styles.jsx";

import { getUsers } from "actions/users.js";

const TIME_PER_LETTER = 500;
const AMOUNT_USERNAMES = 30;

const useInterval = (callback, delay) => {
  const savedCallback = useRef();

  // Remember the latest callback.
  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
};

const UserList = () => {
  const [users, setUsers] = useState(getUsers(AMOUNT_USERNAMES));
  const [styledUsers, setStyledUsers] = useState(users);
  const [letterIndices, setLetterIndices] = useState(
    Array(AMOUNT_USERNAMES).fill(0)
  );

  useInterval(() => {
    users.forEach((user, userIndex) => {
      let styledUser = styledUsers[userIndex];
      let letterIndex = letterIndices[userIndex];
      if (letterIndex === user.length) {
        let newUsername = getUsers(1)[0];

        setUsers((prevUsers) => [
          ...prevUsers.slice(0, userIndex),
          newUsername,
          ...prevUsers.slice(userIndex + 1),
        ]);

        setStyledUsers((prevStyledUsers) => [
          ...prevStyledUsers.slice(0, userIndex),
          newUsername,
          ...prevStyledUsers.slice(userIndex + 1),
        ]);

        setLetterIndices((prevLetterIndices) => [
          ...prevLetterIndices.slice(0, userIndex),
          0,
          ...prevLetterIndices.slice(userIndex + 1),
        ]);
      } else {
        setStyledUsers((prevStyledUsers) => [
          ...prevStyledUsers.slice(0, userIndex),
          [
            ...styledUser.slice(0, letterIndex),
            <span key={letterIndex} style={{ color: "red" }}>
              {user[letterIndex]}
            </span>,
            ...styledUser.slice(letterIndex + 1),
          ],
          ...prevStyledUsers.slice(userIndex + 1),
        ]);

        setLetterIndices((prevLetterIndices) => [
          ...prevLetterIndices.slice(0, userIndex),
          letterIndices[userIndex] + 1,
          ...prevLetterIndices.slice(userIndex + 1),
        ]);
      }
    });
  }, TIME_PER_LETTER);

  return (
    <UserListContainer>
      <UserListList>
        <UserListTitle>Suggested Targets</UserListTitle>
        {styledUsers.map((user, index) => (
          <li key={index}>
            <UserListLink href={"/" + users[index]}>{user}</UserListLink>
          </li>
        ))}
      </UserListList>
    </UserListContainer>
  );
};

export default UserList;
