import React, { useState, useEffect } from "react";

import {
  GlobeContainer,
  GlobePercolationContainer,
  GlobeDigitsContainer,
  GlobeImage,
  GlobeSparks,
} from "assets/styles/Globe.styles.jsx";

import Percolator from "components/Percolator.jsx";

import globe from "assets/images/globe.webp";
import sparks from "assets/images/sparks.webp";

const NUM_DIGIT_LINES = 100;
const DIGITS_PER_LINE = 20;

const Globe = () => {
  const [lines, setLines] = useState([]);

  useEffect(() => {
    const generateDigitLine = () =>
      Array.from({ length: DIGITS_PER_LINE }, () => {
        const spaces = Math.floor(Math.random() * 40); // generate between 0 and 9 spaces
        return Math.floor(Math.pow(Math.random(), 2) * 10) + " ".repeat(spaces); // bias towards lower numbers
      }).join("");

    setLines([...Array(NUM_DIGIT_LINES)].map(generateDigitLine));

    const interval = setInterval(() => {
      setLines((prevLines) => [
        generateDigitLine(),
        ...prevLines.slice(0, NUM_DIGIT_LINES - 1),
      ]);
    }, 75);

    return () => clearInterval(interval);
  }, []);

  return (
    <GlobeContainer>
      <GlobeDigitsContainer>
        {lines.map((line, index) => (
          <div key={index}>{line}</div>
        ))}
      </GlobeDigitsContainer>
      <GlobePercolationContainer>
        <Percolator
          words={["the", "computer", "is", "a", "universal", "machine"]}
          numActiveWords={1000}
          baselineSpeed={1000000}
          speedExponent={3}
        />
      </GlobePercolationContainer>
      <GlobeImage src={globe} />
      <GlobeSparks src={sparks} />
    </GlobeContainer>
  );
};

export default Globe;
