// based on https://raw.githubusercontent.com/mrmaxguns/wonderwordsmodule/master/wonderwords/assets

import adjectives from "assets/words/adjectives.js";
import nouns from "assets/words/nouns.js";
import verbs from "assets/words/verbs.js";
import stats from "assets/words/stats.js";

export { adjectives, nouns, verbs, stats };
