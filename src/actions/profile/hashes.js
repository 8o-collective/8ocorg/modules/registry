const KNOWNDESCRIPTIONS = {
  6321005506514727: "The Operator", // Octopirate
  3463980293776890: "Curator Of Arcane Wisdom", // Snaxolas
  560282472625195: "The Proxy", // Proxy
  3762098211656919: "Man Beyond The Void", // abbatoirsCreed
  6245686317111490: "Gemini Of A Clay Machine", // Snow
  8299878165630987: "Crap Excavation Officer", // TrickleJest
};

// plug this func into node to get hashes for known users

const cyrb53 = function (str, seed = 0) {
  let h1 = 0xdeadbeef ^ seed,
    h2 = 0x41c6ce57 ^ seed;
  for (let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i);
    h1 = Math.imul(h1 ^ ch, 2654435761);
    h2 = Math.imul(h2 ^ ch, 1597334677);
  }
  h1 =
    Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^
    Math.imul(h2 ^ (h2 >>> 13), 3266489909);
  h2 =
    Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^
    Math.imul(h1 ^ (h1 >>> 13), 3266489909);
  return 4294967296 * (2097151 & h2) + (h1 >>> 0);
};

export { cyrb53, KNOWNDESCRIPTIONS };
