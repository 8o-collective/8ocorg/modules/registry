import randy from "randy";

import { stats } from "./wordList.js";
import { chooseDataPointFromMap } from "./mapData.js";
import { cyrb53, KNOWNDESCRIPTIONS } from "./hashes.js";
import generateUsername from "./generateUsername.js";

const map = JSON.parse(localStorage.getItem("map"));

// https://stackoverflow.com/a/11832950
const roundFloat = (num, numDecimals) =>
  Math.round((num + Number.EPSILON) * 10 ** numDecimals) / 10 ** numDecimals;

const generateCoords = () => {
  let [longitudeIndex, latitudeIndex] = chooseDataPointFromMap(map);

  let normalizedLatitudeIndex = 0.5 - latitudeIndex / map.height;
  let normalizedLongitudeIndex = 0.5 - longitudeIndex / map.width;

  let latitude = normalizedLatitudeIndex * 180;
  let longitude = normalizedLongitudeIndex * 360;

  let latitudeDegrees = Math.round(latitude);
  let longitudeDegrees = Math.round(longitude);

  let latitudeMinutes = Math.abs(Math.round((latitude % 1) * 60));
  let longitudeMinutes = Math.abs(Math.round((longitude % 1) * 60));

  let latitudeSeconds = roundFloat(randy.uniform(0, 60), 2);
  let longitudeSeconds = roundFloat(randy.uniform(0, 60), 2);

  let latitudeDirection = latitudeDegrees > 0 ? "N" : "S";
  let longitudeDirection = longitudeDegrees > 0 ? "W" : "E";

  return {
    dms: `${Math.abs(
      parseInt(latitudeDegrees)
    )}° ${latitudeMinutes}’ ${latitudeSeconds}” ${latitudeDirection}, ${Math.abs(
      parseInt(longitudeDegrees)
    )}° ${longitudeMinutes}’ ${longitudeSeconds}” ${longitudeDirection}`,
    decimal: [latitude, -longitude], // dw about this. be cool about it
  };
};

const boxMuller = (min, max, mirror = true) => {
  var u = 0;
  var v = 0;
  while (u === 0) u = randy.random(); //Converting [0,1) to (0,1)
  while (v === 0) v = randy.random();

  let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);

  num = num / 7 + 0.85; // Translate to 0 -> 1
  if (num > 1 || num < 0) return boxMuller(min, max);
  num *= max - min; // Stretch to fill range
  num += min; // offset to min
  if (randy.random() > 0.5 && mirror) {
    num = min - num + max;
  }
  return num;
};

const generateRelated = () => {
  let users = [];
  let numRelated = randy.choice([3, 4, 5, 6, 7]);
  for (let index = 0; index < numRelated; index++) {
    users.push({
      name: generateUsername(),
      valence: roundFloat(boxMuller(-100, 100), 2),
      intensity: roundFloat(boxMuller(-50, 100, false), 2),
    });

    if (users[index].valence >= 0) {
      users[index].valence = " " + String(users[index].valence);
    }

    while (users[index].intensity < 0) {
      users[index].intensity = roundFloat(boxMuller(-50, 100, false), 2);
    }
  }
  users.sort(function (a, b) {
    return b.intensity - a.intensity;
  });

  for (let index = 0; index < 7 - numRelated; index++) {
    users.push({
      name: "",
      valence: "",
      intensity: "",
    });
  }
  return users;
};

const generateStats = (username) => {
  if (username === localStorage.getItem("keyname")) {
    return undefined;
  }

  let seed = cyrb53(username);

  if (seed in KNOWNDESCRIPTIONS) {
    return null;
  }

  randy.setState({ seed: Array.from(Array(32)).map(() => seed), idx: 1 }); // add determinism since all usernames need to have exact stats

  let statNames = randy.sample(stats, 3);
  let secondsSinceStart = Math.round(
    (new Date() - new Date(2021, 3, 14, 0, 0, 0, 0)) / 1000
  ); // start of 8oC in seconds
  let timeSeconds = Math.round(randy.uniform(0, secondsSinceStart));

  return {
    coordinates: generateCoords(),
    qualities: {
      [statNames[0]]: roundFloat(boxMuller(0, 100), 2),
      [statNames[1]]: roundFloat(boxMuller(0, 100), 2),
      [statNames[2]]: roundFloat(boxMuller(0, 100), 2),
    },
    relatedUsers: generateRelated(),
    memberFor: timeSeconds,
  };
};

export default generateStats;
