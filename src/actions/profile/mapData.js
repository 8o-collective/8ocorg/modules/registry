import randy from "randy";

import jpeg from "jpeg-js";

import mapImage from "url-loader!assets/map.jpg";

const BASE64_MARKER = ";base64,";
const convertDataURIToBinary = (dataURI) => {
  let base64 = dataURI.substring(
    dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length
  );
  let raw = window.atob(base64);
  return new Uint8Array(new ArrayBuffer(raw.length)).map((_, i) =>
    raw.charCodeAt(i)
  );
};

const THRESHOLD = 150;

/*
  Map installed from https://earthobservatory.nasa.gov/features/NightLights, specifically the 2012 map located at
  https://eoimages.gsfc.nasa.gov/images/imagerecords/144000/144896/BlackMarble_2012_01deg_gray.jpg
*/

// USING POPULATION MAP
if (!localStorage.getItem("map")) {
  console.log("generating map");
  console.log(mapImage);

  const mapParsed = jpeg.decode(convertDataURIToBinary(mapImage));

  const mapFlat = Array.from(mapParsed.data)
    .filter((e, i) => i % 4 === 0)
    .map(
      (e, i) =>
        e > THRESHOLD && {
          dimensions: [i % mapParsed.width, Math.floor(i / mapParsed.width)],
          value: (e / 255).toFixed(2),
        }
    )
    .filter((e) => e !== false);

  const totalWeight = mapFlat.reduce(
    (partialWeight, e) => partialWeight + parseFloat(e.value),
    0
  );

  const map = {
    height: mapParsed.height,
    width: mapParsed.width,
    data: mapFlat,
    sum: totalWeight,
  };

  console.log(new Blob([JSON.stringify(mapFlat)]).size);

  localStorage.setItem("map", JSON.stringify(map));
}

const chooseDataPointFromMap = (map) => {
  // console.log(totalWeight);
  // console.log(mapFlat);

  console.log(map);

  let threshold = randy.random() * map.sum;

  let currentWeight = 0;

  for (const e of randy.shuffle(map.data)) {
    currentWeight += parseFloat(e.value);

    if (currentWeight >= threshold) return e.dimensions;
  }
};

export { chooseDataPointFromMap };
