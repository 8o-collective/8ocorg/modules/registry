import generateWords from "./generateWords.js";

import randomWords from "random-words";
import randy from "randy/lib/randy.js";

const maybeCapitalize = (string) =>
  randy.random() < 0.5
    ? string.charAt(0).toUpperCase() + string.slice(1)
    : string;

const generateRandomWord = () => {
  return randomWords.wordList[
    Math.floor(randy.random() * randomWords.wordList.length)
  ];
};

const randomWordWithMaxLength = (maxLength) => {
  var rightSize = false;
  var wordUsed;
  while (!rightSize) {
    wordUsed = generateRandomWord();
    if (wordUsed.length <= maxLength) {
      rightSize = true;
    }
  }
  return wordUsed;
};

const generateUsername = () => {
  // console.log(randomWords.wordList)
  let username;

  // Possibilities: 1. a made up word, 2. two real words, 3. a combination of real and made up words, and additionally numbers

  if (randy.random() < 0.8) {
    username = maybeCapitalize(randomWordWithMaxLength(6));

    if (randy.random() < 0.7) {
      username += maybeCapitalize(randomWordWithMaxLength(5));
    } else {
      username += maybeCapitalize(generateWords()[0]);
    }
  } else {
    username = maybeCapitalize(generateWords()[0]);
  }

  if (randy.random() < 0.05) {
    username += Math.floor(randy.random() * 10);
  }

  if (randy.random() < 0.05) {
    username += Math.floor(randy.random() * 10);
  }

  return username;
};

export default generateUsername;
