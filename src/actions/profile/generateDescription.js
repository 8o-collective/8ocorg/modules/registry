import Sentencer from "sentencer"; // I LOVE this library <3 <3 <3
import randy from "randy";

import { adjectives, nouns, verbs } from "./wordList.js";
import { cyrb53, KNOWNDESCRIPTIONS } from "./hashes.js";

Sentencer.configure({
  adjectiveList: adjectives,
  nounList: nouns,

  actions: {
    verb: () => {
      let verb = randy.choice(verbs);
      if (verb.slice(-1) === "e") {
        return verb.slice(0, -1);
      }
      return verb;
    },
  },
});

const patterns = [
  "The {{ verb }}er",
  "The {{ adjective }} {{ noun }}",
  "{{ adjective }} {{ noun }}",
  "The {{ noun }} of {{ nouns }}",
  "{{ a_noun }} of {{ an_adjective }} {{ noun }}",
  "The {{ noun }} of {{ noun }}",
  "{{ noun }} of the {{ nouns }}",
  "{{ noun }} of the {{ adjective }} {{ nouns }}",
  "{{ noun }} of the {{ adjective }} {{ noun }}",
  "{{ noun }} of {{ an_adjective }} {{ noun }}",
  "One of the {{ noun }}-{{ noun }}s",
  "The {{ verb }}ed {{ noun }}",
  "The {{ adjective }} {{ noun }} that {{ verb }}ed",
  "{{ verb }}er",
  "The {{ verb }}er",
  "The {{ verb }}er of {{ verb }}ers",
  "{{ an_adjective }} {{ verb }}er",
  "The {{ adjective }} {{ verb }}er",
  "{{ verb }}er of all {{ adjective }} {{ nouns }}",
  "{{ verb }}er of all {{ adjective }} {{ noun }}s and {{ noun }}s",
  "The {{ noun }}-{{ verb }}er",
  "The {{ adjective }} {{ noun }} {{ verb }}er",
];

const generateDescription = (username) => {
  if (username === localStorage.getItem("keyname")) {
    return null;
  }

  let seed = cyrb53(username);

  if (seed in KNOWNDESCRIPTIONS) {
    return KNOWNDESCRIPTIONS[seed];
  }

  randy.setState({ seed: Array.from(Array(32)).map(() => seed), idx: 1 }); // add determinism since all usernames need to have exact description

  let pattern = randy.choice(patterns);

  let sentence = Sentencer.make(pattern);

  let words = sentence.split(" ");
  for (let i = 0; i < words.length; i++) {
    if (words[i].includes("-")) {
      let wordswords = words[i].split("-");
      words[i] = [
        wordswords[0][0].toUpperCase() + wordswords[0].substr(1),
        wordswords[1][0].toUpperCase() + wordswords[1].substr(1),
      ].join("-");
    }
    words[i] = words[i][0].toUpperCase() + words[i].substr(1);
  }

  return words.join(" ");
};

export default generateDescription;
