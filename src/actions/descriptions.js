import generateDescription from "./profile/generateDescription";

const getDescription = (username) => generateDescription(username);

export { getDescription };
