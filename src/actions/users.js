import generateUsername from "./profile/generateUsername";

const getUsers = (numUsernames) =>
  Array.from(Array(numUsernames)).map(() => generateUsername());

export { getUsers };
