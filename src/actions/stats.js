import generateStats from "./profile/generateStats";

const getStats = (username) => generateStats(username);

export { getStats };
