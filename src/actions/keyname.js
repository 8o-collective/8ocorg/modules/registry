import generateUsername from "./profile/generateUsername";

const generateKeyname = () =>
  localStorage.setItem("keyname", generateUsername());

export { generateKeyname };
