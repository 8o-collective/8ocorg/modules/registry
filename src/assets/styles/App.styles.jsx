import styled from "styled-components";

const AppContainer = styled.div`
  background-color: black;

  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0%;
  display: flex;
  align-items: center;
  flex-direction: column;
  font-size: max(20px, 2vh);

  overflow: hidden;

  @media (max-width: 768px) {
    width: 90%;
    left: 5%;

    overflow: visible;
  }
`;

const AppWelcomeContainer = styled.div`
  position: static;
  display: flex;
  align-items: left;
  flex-direction: column;
  margin-top: 5vh;
  width: fit-content;
  top: 50vh;

  font-family: "Times Pixelated 32";
  color: white;
`;

const AppWelcomeTitle = styled.div`
  font-size: max(40px, 7vh, 4vw);
`;

const AppWelcomeDescription = styled.div`
  margin-top: 1%;
  float: left;
  font-family: "Times Pixelated 16";
  color: white;
`;

const AppWelcomeStrong = styled.strong`
  color: red;
`;

export {
  AppContainer,
  AppWelcomeContainer,
  AppWelcomeTitle,
  AppWelcomeDescription,
  AppWelcomeStrong,
};
