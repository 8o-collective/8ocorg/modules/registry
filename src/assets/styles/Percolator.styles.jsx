import styled, { keyframes } from "styled-components";

const PercolatorContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;

const riseAnimation = () => keyframes`
	// from { bottom: 0%; }
	to { bottom: 100%; }
`;

const moveAnimation = () => keyframes`
	// from { left: 0%; }
	to { right: 100%; }
`;

const PercolatorWord = styled.div`
  position: absolute;
  bottom: 0px;
  animation-name: ${(props) =>
    props.direction === "left" ? moveAnimation : riseAnimation};
  animation-timing-function: linear;
  animation-iteration-count: 1;
  animation-fill-mode: forwards;

  color: red;
  font-size: 10px;
  font-family: "Courier Pixelated 16";
`;

export { PercolatorContainer, PercolatorWord };
