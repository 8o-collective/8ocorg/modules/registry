import styled from "styled-components";

const GlobeContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 25vw;
  height: 100vh;

  color: white;

  pointer-events: none;
`;

const GlobePercolationContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 20%;
  width: 60%;
  height: 100%;

  transform: translateX(-15%);
`;

const GlobeDigitsContainer = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;

  white-space: break-spaces;

  overflow-y: hidden;

  color: darkred;
  font-size: 10px;
  font-family: "Courier Pixelated 16";
`;

const GlobeImage = styled.img`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 200%;

  transform: translate(-28%, -15%);

  overflow-y: hidden;
`;

const GlobeSparks = styled.img`
  position: absolute;
  bottom: 0px;
  left: 0px;

  width: 100%;

  transform: translate(0%, 70%);

  overflow-y: hidden;
`;

export {
  GlobeContainer,
  GlobePercolationContainer,
  GlobeDigitsContainer,
  GlobeImage,
  GlobeSparks,
};
