import styled from "styled-components";

const UserListContainer = styled.div`
  position: static;
  display: flex;
  justify-content: center;
  margin: 0px;
  left: 75vw;
  width: 100%;

  font-family: "Times Pixelated 24";
  color: white;
`;

const UserListLink = styled.a`
  font-family: "Times Pixelated 16";
  color: white;
`;

const UserListList = styled.ul`
  column-count: 2;
  column-gap: max(40px, 2vh);
  column-width: 250px;
  margin-left: 40px;
  max-width: 600px;
  word-break: break-all;
`;

const UserListTitle = styled.div`
  column-span: all;
  font-size: max(38px, 4.5vh);
  margin-bottom: 2vh;
  word-break: initial;
`;

export { UserListContainer, UserListLink, UserListList, UserListTitle };
