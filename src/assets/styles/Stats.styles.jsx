import React from "react";
import styled from "styled-components";

const StatsContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: static;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;

  display: flex;
  align-items: flex-start;
  flex-wrap: wrap;
  flex-direction: column;
  height: 56vh;
  align-content: flex-start;

  @media (max-width: 768px) {
    height: max-content;
  }
`;

const StatsTileHeader = styled.header`
  font-size: max(4vh, 20px);
  font-weight: bold;
  font-family: "Courier Pixelated 24 Bold";
  margin-bottom: 0.3vh;
`;

const StatsAttributeContainer = styled.div`
  padding-left: max(1.5vw, 10px);
  text-align: left;
  background-color: #1c1c1c;
`;

const StatsCoordinatesContainer = styled(StatsAttributeContainer)`
  margin-top: max(1vh, 9px);
  line-height: max(4vh, 20px);
  overflow: hidden;
  text-overflow: ellipsis;
  width: calc(100%);
`;

const StatsCoordinatesHyperlink = styled.a`
  color: red;
`;

const StatsBarlabelContainer = styled.div`
  line-height: max(4vh, 20px);
`;

const StatsBarContainer = styled.div`
  display: inline-block;
  top: 50%;
  padding: 2px;
  width: 100%;
  border: min(3px, 0.7vh) solid ${(props) => props.color};
  height: max(17px, 3vh);
`;

const StatsBar = ({ color, progress }) => (
  <StatsBarContainer color={color}>
    <div
      style={{ height: "100%", backgroundColor: color, width: `${progress}%` }}
    >
      <div
        style={{
          position: "relative",
          top: "50%",
          transform: "translateY(-50%)",
          marginLeft: ".4vw",
        }}
      >
        {progress}%
      </div>
    </div>
  </StatsBarContainer>
);

const StatsRelatedUserTable = styled.table`
  padding-left: max(1.5vw, 10px);
  margin-right: max(2vw, 17px);
  border-collapse: collapse;
  display: flex;
  background-color: ${(props) => props.color};
  padding-bottom: ${(props) => props.padding};
  flex-basis: 5vh;
  line-height: 5vh;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
`;

const StatsRelatedUserTableHeader = styled.thead``;

const StatsRelatedUserTableBody = styled.tbody``;

const StatsRelatedUserLink = styled.a`
  color: red;
`;

const StatsTableRow = styled.tr`
  text-align: left;
  background-color: ${(props) => props.color};
  flex-basis: ${(props) => props.height || "inherit"};
`;

const StatsTableCell = styled.td`
  width: calc(10vw);
  overflow: hidden;
  display: inline-block;
  text-overflow: ellipsis;
  line-height: normal;

  @media (max-width: 768px) {
    width: calc(30vw);
  }
`;

const StatsTableHeader = styled(StatsTableCell)`
  font-weight: bold;
  vertical-align: text-bottom;
`;

const StatsTableDiv = styled.div`
  margin-bottom: 21px;
  display: flex;
  flex-direction: column;
  order: 1;
  @media (max-width: 768px) {
    order: 1;
    margin-bottom: 30px;
  }
`;

const StatsMainDiv = styled.div`
  order: 3;
  @media (max-width: 768px) {
    order: 2;
  }
`;

const StatsMainContainer = styled(StatsAttributeContainer)`
  padding: 23px 30px;
  height: 40vh;
  width: 55vw;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media (max-width: 768px) {
    width: calc(90vw - 60px);
    height: fit-content;
    margin-bottom: 30px;
  }
`;

const StatsMemberForDiv = styled.div`
  order: 2;
  width: calc(30vw);
  height: 5vh;
  padding: 10px 0px;
  display: flex;
  align-items: center;
  background-color: #1c1c1c;
  @media (max-width: 768px) {
    order: 3;
    width: calc(90vw);
  }
`;

export {
  StatsContainer,
  StatsAttributeContainer,
  StatsCoordinatesContainer,
  StatsCoordinatesHyperlink,
  StatsTileHeader,
  StatsBar,
  StatsBarlabelContainer,
  StatsRelatedUserTable,
  StatsRelatedUserTableHeader,
  StatsRelatedUserTableBody,
  StatsRelatedUserLink,
  StatsTableRow,
  StatsTableCell,
  StatsTableHeader,
  StatsTableDiv,
  StatsMainDiv,
  StatsMainContainer,
  StatsMemberForDiv,
};
