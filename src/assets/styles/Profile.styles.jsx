import styled from "styled-components";

const ProfileContainer = styled.div`
  background-color: black;

  margin: 0px;
  position: absolute;
  width: 95%;
  height: 95%;
  top: 5vh;
  right: 5vw;
  left: 5vw;
  font-size: max(14px, 2vh);
  overflow-x: hidden;
  @media (max-width: 768px) {
    left: 50%;
    transform: translateX(-50%);
  }
`;

const ProfilePercolationContainer = styled.div`
  position: absolute;
  top: 0%;
  right: 0px;
  width: 50%;
  height: 10%;
  transform: translate(5%, 30%);

  @media (max-width: 768px) {
    height: 5%;
    transform: translate(10%, 50%);
  }
`;

const ProfileTitleContainer = styled.div`
  position: static;
  margin: 0px;
  font-family: "Times Pixelated 32";
  text-decoration: underline;
  font-size: max(60px, 10.5vh, 6vw);
  width: 95%;
  color: white;
  word-wrap: break-word;
  @media (max-width: 768px) {
    font-size: max(40px, 7vh, 4vw);
  }
`;

const ProfileDescriptionContainer = styled.div`
  position: static;
  font-style: italic;
  font-family: "Times Pixelated 24";
  font-size: max(4vh, 20px, 3vw);
  color: white;
  margin-top: 4vh;
  white-space: pre-line;
`;

const ProfileStatsContainer = styled.div`
  position: static;
  margin-top: 8vh;

  font-family: "Courier Pixelated 16";
  color: white;
  white-space: pre;
`;

const ProfileBackLink = styled.a`
  position: static;
  display: block;
  width: fit-content;
  align-items: center;

  padding: 6px;
  padding-right: 7px;
  font-family: "Times Pixelated 16";
  color: red;
  font-size: max(14px, 2vh);
  margin-left: 85vw;
  @media (max-width: 768px) {
    visibility: hidden;
  }
`;

export {
  ProfileContainer,
  ProfilePercolationContainer,
  ProfileTitleContainer,
  ProfileDescriptionContainer,
  ProfileStatsContainer,
  ProfileBackLink,
};
