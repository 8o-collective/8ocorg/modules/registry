import styled from "styled-components";

const SearchContainer = styled.div`
  position: static;
  margin-top: min(4.5vh, 50px);
  margin-bottom: min(1vh, 50px);
  width: max(400px, 50%);
  display: flex;
  justify-content: center;
`;

const SearchInput = styled.input`
  font-family: "Times Pixelated 16";
  font-size: max(20px, 2vh);
  padding: 9px;
  width: 100%;
  color: red;
  background-color: black;
  border: 1px red solid;

  outline: none;
`;

const SearchInputLabel = styled.label`
  border-left: 0px;
  width: 100%;
`;

const SearchInputSubmit = styled(SearchInput)`
  padding: 0px 25px;
  width: fit-content;
  border-left: none;
`;

const SearchForm = styled.form`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export {
  SearchContainer,
  SearchInput,
  SearchInputLabel,
  SearchInputSubmit,
  SearchForm,
};
